/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Instant;
import modelo.*;
import vista.JifProductos;
import javax.swing.JOptionPane;

import javax.swing.table.DefaultTableModel;

//manejo de fechas
import java.util.Date;
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;

/**
 *
 * @author Jose Figueroa
 */
public class Controlador implements ActionListener{
    private JifProductos vista;
    private dbProducto db;
    private boolean EsActualizar;
    private int idProducto = 0;

    
    public Controlador(JifProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        this.deshabilitar();
        
        
        
    }
    
    //helpers
    
    public void limpiar(){
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.dtfFecha.setDate(null);
    }
    
    public void cerrar(){
        int res = JOptionPane.showConfirmDialog(vista,"Deseas cerrar el sistema","Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if(res == JOptionPane.YES_OPTION){
           vista.dispose();
        }
    }
    
    public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.dtfFecha.setEnabled(true);

        vista.btnDeshabilitar.setEnabled(true);
        vista.btnHabilitar.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnLimpiar.setEnabled(true);
        vista.btnCancelar.setEnabled(true);
        
    }
    
    public void deshabilitar(){
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.dtfFecha.setEnabled(false);

        vista.btnDeshabilitar.setEnabled(false);
        vista.btnHabilitar.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnLimpiar.setEnabled(false);
        vista.btnCancelar.setEnabled(false);
        vista.lista.setEnabled(false);
        
    }
    
    public boolean validar(){
        boolean exito = true;
        
        if(vista.txtCodigo.getText().equals("") || vista.txtNombre.getText().equals("")|| vista.txtPrecio.getText().equals("") || vista.dtfFecha.getDate()== null) exito = false;
        
        return exito;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource()==vista.btnLimpiar){
            this.limpiar();
        }
        if(ae.getSource()==vista.btnCancelar){
            this.limpiar();
            this.deshabilitar();
        }
        if(ae.getSource()==vista.btnCerrar){
            this.cerrar();
        }
        if(ae.getSource()==vista.btnNuevo){
            this.habilitar();
            this.EsActualizar = false;
        }
        
       if (ae.getSource() == vista.btnGuardar) {
        // Validar
        if (this.validar()) {
            Productos pro = new Productos();
            pro.setCodigo(vista.txtCodigo.getText());
            pro.setNombre(vista.txtNombre.getText());
            pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
            pro.setStatus(0);
            pro.setFecha(this.convertirAñoMesDia(vista.dtfFecha.getDate()));

            try {
                if (this.EsActualizar) {
                    db.actualizar(pro);
                    JOptionPane.showMessageDialog(vista, "Se actualizó con éxito el producto");
                        this.limpiar();
                        this.deshabilitar();
                        this.ActualizarTabla(db.lista());
                } else {
                    if (!db.siExiste(pro.getCodigo())) {
                         db.insertar(pro);
                        JOptionPane.showMessageDialog(vista, "Se agregó con éxito el producto");
                        this.limpiar();
                        this.deshabilitar();
                        this.ActualizarTabla(db.lista());
                    } else {
                        JOptionPane.showMessageDialog(vista, "Ya hay un producto con ese código");
                    }
                }
            } catch(Exception e) {
                 JOptionPane.showMessageDialog(vista, "Surgió un error: " + e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista, "Faltaron datos por insertar");
            }
        }

        //deshabilitar
        
        if(ae.getSource()== vista.btnDeshabilitar){
            if(vista.txtCodigo.getText().equals("")){
            JOptionPane.showMessageDialog(vista, "Falto ingresar el codigo del producto por deshabilitar");
            }
            else{
                int opc = 0;
            opc = JOptionPane.showConfirmDialog(vista, "Deseas deshabilitar el producto", "Producto",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            
            if(opc == JOptionPane.YES_OPTION){
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                
                try{
                   db.deshabilitar(pro);
                   JOptionPane.showMessageDialog(vista, "El producto fue deshabilitado");
                   this.limpiar();
                   this.ActualizarTabla(db.lista());
                   
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
                }
            }
            }
            
        }
        
        //habilitar
        if(ae.getSource()== vista.btnHabilitar){
        
            if(vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(vista, "Falto ingresar el codigo del producto por habilitar");
            }
            
            else{
                int opc = 0;
            opc = JOptionPane.showConfirmDialog(vista, "Deseas habilitar el producto", "Producto",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            
            if(opc == JOptionPane.YES_OPTION){
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                
                try{
                    db.habilitar(pro);
                    JOptionPane.showMessageDialog(vista, "El producto fue habilitado");
                    this.limpiar();
                    this.ActualizarTabla(db.lista());
                    
                } catch(Exception e){
                    JOptionPane.showMessageDialog(vista,"Surgio un error." + e.getMessage());
                }
            }
            }
        }
        
        
        if(ae.getSource()==vista.btnBuscar){
            Productos pro = new Productos();
            
            if(vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(vista, "Falto capturar el codigo");
            } else{
                try{
                    pro = (Productos)db.buscar(vista.txtCodigo.getText());
                   if(pro.getIdProductos()!=0){
                       
                    vista.txtNombre.setText(pro.getNombre());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    convertirStringDate(pro.getFecha());
                    this.EsActualizar = true;
                    vista.btnDeshabilitar.setEnabled(true);
                    vista.btnGuardar.setEnabled(true);
                   } else JOptionPane.showMessageDialog(vista, "No se encontro");
                   
                } catch(Exception e){
                    JOptionPane.showMessageDialog(vista,"Surgio un error" + e.getMessage());
                }
            }
        }
    }
    
    public void ActualizarTabla(ArrayList<Productos>arr){
        String campos[]= {"IdProductos","Codigo","Nombre","Precio","Fecha"};        
        String [][] datos = new String[arr.size()][5];
        int renglon = 0;
        
        for (Productos registro : arr){
            datos[renglon][0] = String.valueOf(registro.getIdProductos());
            datos[renglon][1] = registro.getCodigo();
            datos[renglon][2] = registro.getNombre();
            datos[renglon][3] = "$" + String.valueOf(registro.getPrecio());
            datos[renglon][4] = registro.getFecha();
            
            renglon ++;
        }
        DefaultTableModel tb = new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
        vista.lista.getTableHeader().setEnabled(false);
    }
    
    public void IniciarVista(){
        vista.setTitle("Productos");
        vista.resize(900,600);
        vista.setVisible(true);
        try{
            this.ActualizarTabla(db.lista());
        }catch(Exception e){
            JOptionPane.showMessageDialog(vista,"Surgio un error." +  e.getMessage());
        }        
    }
    
    public String convertirAñoMesDia(Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    
    public void convertirStringDate(String fecha){
        try{
            //convertir la cadena de texto a un objeto Date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.dtfFecha.setDate(date);
        } catch (Exception e){
            System.err.print(e.getMessage());
        }
    }           
}
